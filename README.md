# Node application starter for Clever Cloud

## What?

This project is a starter kit to create and deploy easily an **Express** application on [Clever Cloud](https://www.clever-cloud.com/en/).

Since the moment you fork this project you can deploy the application on **Clever Cloud** and create [reviews applications](https://about.gitlab.com/features/review-apps/)

## Setup

- First, fork this project
- Add these 4 environment variables to the CI/CD settings of your project (or group)
  - `CLEVER_SECRET`
  - `CLEVER_TOKEN`
  - `ORGANIZATION_ID`
  - `ORGANIZATION_NAME` *(optional)*

## Update the `.gitlab-ci.yml`

Change the values of these 2 variables with your values

```yaml
variables:
  CLEVER_APPLICATION_NAME: "node-app-demo"
  CLEVER_APPLICATION_DOMAIN: "node-app-demo"
```

